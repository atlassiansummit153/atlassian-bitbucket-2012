# Galician translation of Python Sudoku
# Copyright (C) 2005, 2006
# Xosé Otero <xoseotero@users.sourceforge.net>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: Python Sudoku 0.12\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-09-19 01:37+0200\n"
"PO-Revision-Date: 2008-09-18 23:34+0200\n"
"Last-Translator: Xosé Otero <xoseotero@users.sourceforge.net>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /usr/lib/python2.4/optparse.py:332
#, python-format
msgid "usage: %s\n"
msgstr "uso: %s\n"

#: /usr/lib/python2.4/optparse.py:351
msgid "Usage"
msgstr "Uso"

#: /usr/lib/python2.4/optparse.py:357
msgid "integer"
msgstr "número enteiro"

#: /usr/lib/python2.4/optparse.py:358
msgid "long integer"
msgstr "número enteiro longo"

#: /usr/lib/python2.4/optparse.py:359
msgid "floating-point"
msgstr "número de coma flotante"

#: /usr/lib/python2.4/optparse.py:360
msgid "complex"
msgstr "número complexo"

#: /usr/lib/python2.4/optparse.py:368
#, python-format
msgid "option %s: invalid %s value: %r"
msgstr "opción %s: o valor %s non é válido: %r"

#: /usr/lib/python2.4/optparse.py:376
#, python-format
msgid "option %s: invalid choice: %r (choose from %s)"
msgstr "opción %s: escolla non válida: %r (escolla entre %s)"

#: /usr/lib/python2.4/optparse.py:1141
msgid "show this help message and exit"
msgstr "amosar esta mensaxe de axuda e saír"

#: /usr/lib/python2.4/optparse.py:1146
msgid "show program's version number and exit"
msgstr "amosa-lo número da versión do programa e saír"

#: /usr/lib/python2.4/optparse.py:1169
msgid "%prog [options]"
msgstr "%prog [opcións]"

#: /usr/lib/python2.4/optparse.py:1379 /usr/lib/python2.4/optparse.py:1418
#, python-format
msgid "%s option requires an argument"
msgstr "a opción %s precisa un argumento"

#: /usr/lib/python2.4/optparse.py:1381 /usr/lib/python2.4/optparse.py:1420
#, python-format
msgid "%s option requires %d arguments"
msgstr "a opción %s precisa %d argumentos"

#: /usr/lib/python2.4/optparse.py:1390
#, python-format
msgid "%s option does not take a value"
msgstr "a opción %s non toma ningún valor"

#: /usr/lib/python2.4/optparse.py:1407 /usr/lib/python2.4/optparse.py:1561
#, python-format
msgid "no such option: %s"
msgstr "non hai tal opción: %s"

#: /usr/lib/python2.4/optparse.py:1507
msgid "options"
msgstr "opcións"

#: /usr/lib/python2.4/optparse.py:1565
#, python-format
msgid "ambiguous option: %s (%s?)"
msgstr "opción ambigua: %s (%s?)"

#: pysdk-gui.py:83
msgid ""
"\n"
"  %prog [GUI Options] [Print Options] [PDF Options] [Image Options] [INPUT.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file"
msgstr ""
"\n"
"  %prog [Opcións gráficas] [Opcións de impresión] [Opcións de PDF] [Opción de imaxe] [ENTRADA.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  ENTRADA.sdk é un ficheiro de Python Sudoku"

#: pysdk-gui.py:92
msgid "show the modules not found"
msgstr "amosa-los módulos non atopados"

#: pysdk-gui.py:96 pysdk-image.py:92 pysdk-pdf.py:98 pysdk.py:100
msgid "information about what is sudoku"
msgstr "información acerca de que é un sudoku"

#: pysdk-gui.py:99 pysdk.py:103
msgid "Creation Options"
msgstr "Opcións de creación"

#: pysdk-gui.py:103 pysdk.py:107
#, python-format
msgid "set the difficulty of the sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" is the default)"
msgstr "establece-la dificultade do sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:107 pysdk.py:114
#, python-format
msgid "set the handicap of the sudoku (0 = insane, 1 = insane + 1 extra number, etc) (%d is the default)"
msgstr "establece-lo handicap do sudoku (0 = moi difícil, 1 = moi difícil + 1 número máis, etc) (%d é o valor por defecto)"

#: pysdk-gui.py:111 pysdk.py:118
#, python-format
msgid "set the region width. The board will be HxW grid of WxH grids (%d is the default)"
msgstr "establece-lo ancho da rexión. O taboeiro será Alto x Ancho rexións por Ancho x Alto celas (%d é o valor por defecto)"

#: pysdk-gui.py:115 pysdk.py:122
#, python-format
msgid "set the region height. The board will be HxW grid of WxH grids (%d is the default)"
msgstr "establece-lo alto da rexión. O taboeiro será Alto x Ancho rexións por Ancho x Alto celas (%d é o valor por defecto)"

#: pysdk-gui.py:119 pysdk-pdf.py:164
msgid "Print Options"
msgstr "Opcións de Impresión"

#: pysdk-gui.py:121 pysdk-pdf.py:166
msgid "set the command to print (not value set)"
msgstr "establece-lo comando para imprimir (non hai ningún valor establecido)"

#: pysdk-gui.py:122 pysdk-pdf.py:167
#, python-format
msgid "set the command to print (\"%s\" is the default)"
msgstr "establece-lo comando para imprimir (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:130
msgid "Print and PDF Options"
msgstr "Opcións de Impresión e PDF"

#: pysdk-gui.py:134 pysdk-pdf.py:105
#, python-format
msgid "set the page size (\"A4\", \"LEGAL\", \"LETTER\", etc) (\"%s\" is the default)"
msgstr "establece-lo tamaño da páxina (\"A4\", \"LEGAL\", \"LETTER\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:138 pysdk-pdf.py:109
msgid "don't draw the title text"
msgstr "non debuxa-lo texto do título"

#: pysdk-gui.py:142 pysdk-pdf.py:113
#, python-format
msgid "set the title font (\"%s\" is the default)"
msgstr "establece-la fonte do título (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:146 pysdk-pdf.py:117
#, python-format
msgid "set the title colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "establece-la cor do título (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:150 pysdk-pdf.py:121
#, python-format
msgid "set the title font size (%d is the default)"
msgstr "establece-lo tamaño da fonte do título (%d é o valor por defecto)"

#: pysdk-gui.py:154 pysdk-pdf.py:125
msgid "don't draw the filename"
msgstr "non amosa-lo nome de arquivo"

#: pysdk-gui.py:158 pysdk-pdf.py:129
#, python-format
msgid "set the filename font (\"%s\" is the default)"
msgstr "establece-la fonte do nome de arquivo (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:162 pysdk-pdf.py:133
#, python-format
msgid "set the filename colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "establece-la cor do nome de arquivo (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:166 pysdk-pdf.py:137
#, python-format
msgid "set the filename size (%d is the default)"
msgstr "establece-lo tamaño do nome de arquivo (%d é o valor por defecto)"

#: pysdk-gui.py:169 pysdk-pdf.py:140
msgid "show 4 sudokus instead of 1"
msgstr "amosar 4 sudokus no lugar de 1"

#: pysdk-gui.py:173 pysdk-pdf.py:144
msgid "show valids fonts"
msgstr "amosa-las fontes válidas"

#: pysdk-gui.py:177 pysdk-image.py:95
msgid "Image Options"
msgstr "Opcións de Imaxe"

#: pysdk-gui.py:181 pysdk-image.py:99
#, python-format
msgid "set the image format (\"png\", \"jpeg\", etc) (\"%s\" is the default)"
msgstr "establece-lo formato da imaxe (\"png\", \"jpeg\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:185 pysdk-image.py:103
#, python-format
msgid "set the image width in pixels (%d is the default)"
msgstr "establece-lo ancho en pixels da imaxe (%d é o valor por defecto)"

#: pysdk-gui.py:189 pysdk-image.py:107
#, python-format
msgid "set the image height in pixels (%d is the default)"
msgstr "establece-lo alto en pixels da imaxe (%d é o valor por defecto)"

#: pysdk-gui.py:193 pysdk-image.py:111
msgid "create a transparent image without background (if the format doesn't support transparency a black background is created)"
msgstr "crear unha imaxe transparente sen fondo (se o formato non soporta transparencia un fondo negro é creado)"

#: pysdk-gui.py:197 pysdk-image.py:115
#, python-format
msgid "set the image background (\"white\", \"blue\", etc) (\"%s\" is the default)"
msgstr "establece-lo fondo da imaxe (\"white\", \"blue\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-gui.py:202
msgid "Print, PDF and Image Options"
msgstr "Opcións de Impresión, PDF e Imaxe"

#: pysdk-gui.py:207
#, python-format
msgid "set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "establece-la cor das liñas (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto para Imaxe e \"%s\" para PDF/Impresión)"

#: pysdk-gui.py:211
#, python-format
msgid "set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "establece-la fonte para os números (path absoluto ou relativo ó script) (\"%s\" é o valor por defecto para Imaxe e \"%s\" para PDF/Impresión)"

#: pysdk-gui.py:216
#, python-format
msgid "set the font size for the numbers (%d is the default for image and %d for PDF/Print)"
msgstr "establece-lo tamaño da fonte para os números (%d é o valor por defecto para Imaxe e %d para PDF/Impresión)"

#: pysdk-gui.py:221
#, python-format
msgid "set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)"
msgstr "establece-la cor da fonte para os números (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto para Imaxe e \"%s\" para PDF/Impresión)"

#: pysdk-gui.py:224 pysdk-image.py:135 pysdk-pdf.py:174 pysdk.py:125
msgid "Visualization Options"
msgstr "Opcións de visualización"

#: pysdk-gui.py:226 pysdk-image.py:137 pysdk-pdf.py:176 pysdk.py:127
msgid "show letters instead of numbers > 9 (default)"
msgstr "amosar letras no lugar de números > 9 (por defecto)"

#: pysdk-gui.py:227 pysdk-image.py:138 pysdk-pdf.py:177 pysdk.py:128
msgid "show only numbers"
msgstr "amosar só números"

#: pysdk-gui.py:229 pysdk-image.py:140 pysdk-pdf.py:179 pysdk.py:130
msgid "show letters instead of numbers > 9"
msgstr "amosar letras no lugar de números > 9"

#: pysdk-gui.py:230 pysdk-image.py:141 pysdk-pdf.py:180 pysdk.py:131
msgid "show only numbers (default)"
msgstr "amosar só números (por defecto)"

#: pysdk-gui.py:256 pysdk-image.py:167 pysdk-pdf.py:210 pysdk.py:159
msgid "incorrect number of arguments"
msgstr "número de argumentos incorrecto"

#: pysdk-gui.py:342
msgid "You have not psyco installed, if you can, install it to get better performance"
msgstr "Non ten psyco instalado, se pode, instáleo para obter mellor rendemento"

#: pysdk-gui.py:344
msgid "You have not reportlab installed, necessary to save as PDF and printing"
msgstr "Non ten reportlab instalado, necesario para gardar coma PDF e imprimir"

#: pysdk-gui.py:346
msgid "You have not PIL installed, necessary to save as image"
msgstr "Non ten PIL instalado, necesario para gardar coma imaxe"

#: pysdk-gui.py:348
msgid "You have not pygtk installed, necessary to gui"
msgstr "Non ten pygtk instalado, necesario para o gui"

#: pysdk-gui.py:365 pysdk-gui.py:367 pysdk-image.py:213 pysdk-image.py:215
#: pysdk-pdf.py:276 pysdk-pdf.py:278 pysdk.py:207 pysdk.py:209
msgid "\t-- cut here --"
msgstr "\t-- corte aquí --"

#: pysdk-gui.py:369 pysdk-image.py:217 pysdk-pdf.py:280 pysdk.py:211
#, python-format
msgid "An error has happened, please go to %s and send a bug report with the last lines."
msgstr "Ocorreu un erro, por favor, vaia a %s e envíe un informe do erro coas últimas liñas."

#: pysdk-image.py:82
msgid ""
"\n"
"  %prog [Image Options] INPUT.sdk OUTPUT.format\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.format is an image file"
msgstr ""
"\n"
"  %prog [Opcións de imaxe] ENTRADA.sdk SAÍDA.formato\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  ENTRADA.sdk é un ficheiro de Python Sudoku\n"
"  SAÍDA.formato é un ficheiro de imaxe"

#: pysdk-image.py:120 pysdk-pdf.py:149
#, python-format
msgid "set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "establece-la cor das liñas (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-image.py:124 pysdk-pdf.py:153
#, python-format
msgid "set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default)"
msgstr "establece-la fonte para os números (ruta absoluta ou relativa ó scripts) (\"%s\" é o valor por defecto)"

#: pysdk-image.py:128 pysdk-pdf.py:157
#, python-format
msgid "set the font size for the numbers (%d is the default)"
msgstr "establece-lo tamaño de fonte para os números (%d é o valor por defecto)"

#: pysdk-image.py:132 pysdk-pdf.py:161
#, python-format
msgid "set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default)"
msgstr "establece-la cor da fonte para os números (\"black\", \"blue\", etc) (\"%s\" é o valor por defecto)"

#: pysdk-pdf.py:84
msgid ""
"\n"
"  %prog [PDF Options] [-p [Print Options]] INPUT.sdk [INPUT.sdk INPUT.sdk INPUT.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk is a Python Sudoku file\n"
"  OUTPUT.pdf is a PDF file"
msgstr ""
"\n"
"  %prog [Opcións de PDF] [-p [Opcións de impresión]] ENTRADA.sdk [ENTRADA.sdk ENTRADA.sdk ENTRADA.sdk]\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  ENTRADA.sdk é un ficheiro de Python Sudoku\n"
"  SAÍDA.pdf é un ficheiro PDF"

#: pysdk-pdf.py:94
msgid "print a sudoku"
msgstr "imprimir un sudoku"

#: pysdk-pdf.py:101
msgid "PDF Options"
msgstr "Opcións de PDF"

#: pysdk.py:81
msgid ""
"\n"
"  %prog INPUT.sdk [OUTPUT.sdk]\n"
"  %prog -c [Creation Options] OUTPUT.sdk\n"
"  %prog -t INPUT.sdk\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  INPUT.sdk and OUTPUT.sdk are Python Sudoku files"
msgstr ""
"\n"
"  %prog ENTRADA.sdk [SAÍDA.sdk]\n"
"  %prog -c [Opcións de creación] SAÍDA.sdk\n"
"  %prog -t ENTRADA.sdk\n"
"  %prog --version | -h | -m | -w\n"
"\n"
"  ENTRADA.sdk e SAÍDA.sdk son ficheiros de Python Sudoku"

#: pysdk.py:92
msgid "create a sudoku"
msgstr "crear un sudoku"

#: pysdk.py:96
msgid "test the difficulty of a sudoku"
msgstr "comproba-la dificultada dun sudoku"

#: pysdk.py:110
msgid "be sure that the difficulty of the sudoku created is the difficulty given"
msgstr "asegurarse de que a dificultade dun sudoku creado é a dificultade dada"

#: pythonsudoku/gui.py:49
msgid "_Open"
msgstr "_Abrir"

#: pythonsudoku/gui.py:54
msgid "_Save sudoku"
msgstr "_Gardar sudoku"

#: pythonsudoku/gui.py:60
msgid "Save as P_DF"
msgstr "Gardar coma _PDF"

#: pythonsudoku/gui.py:66
msgid "Save as _Image"
msgstr "Gardar coma I_maxe"

#: pythonsudoku/gui.py:73
msgid "_Print"
msgstr "_Imprimir"

#: pythonsudoku/gui.py:78
msgid "_Quit"
msgstr "_Saír"

#: pythonsudoku/gui.py:83
msgid "_File"
msgstr "_Arquivo"

#: pythonsudoku/gui.py:92
msgid "_Undo"
msgstr "_Desfacer"

#: pythonsudoku/gui.py:97
msgid "_Redo"
msgstr "_Refacer"

#: pythonsudoku/gui.py:102
msgid "_Edit"
msgstr "_Editar"

#: pythonsudoku/gui.py:111
msgid "_Create"
msgstr "_Crear"

#: pythonsudoku/gui.py:116
msgid "C_heck"
msgstr "C_omprobar"

#: pythonsudoku/gui.py:121
msgid "_Solve"
msgstr "_Resolver"

#: pythonsudoku/gui.py:126
msgid "_Give one number"
msgstr "_Dar un número"

#: pythonsudoku/gui.py:131
msgid "_Sudoku"
msgstr "_Sudoku"

#: pythonsudoku/gui.py:140
msgid "_About"
msgstr "_Acerda de"

#: pythonsudoku/gui.py:145
msgid "_What is"
msgstr "_Que é"

#: pythonsudoku/gui.py:150
msgid "_Help"
msgstr "A_xuda"

#: pythonsudoku/gui.py:173
msgid "Open file"
msgstr "Abrir arquivo"

#: pythonsudoku/gui.py:183
msgid "Save file"
msgstr "Gardar arquivo"

#: pythonsudoku/gui.py:193
msgid "Save file as PDF"
msgstr "Gardar arquivo coma PDF"

#: pythonsudoku/gui.py:203
msgid "Save file as an image"
msgstr "Gardar arquivo coma unha imaxe"

#: pythonsudoku/gui.py:218
msgid "Select a number"
msgstr "Seleccione un número"

#: pythonsudoku/gui.py:394
msgid "Create sudoku"
msgstr "Crear sudoku"

#: pythonsudoku/gui.py:398
msgid "Select your handicap"
msgstr "Seleccione o seu handicap"

#: pythonsudoku/gui.py:439 pythonsudoku/text.py:76
msgid "Creating sudoku..."
msgstr "Creando sudoku..."

#: pythonsudoku/gui.py:616
msgid "Solved!"
msgstr "¡Resolto!"

#: pythonsudoku/gui.py:648
msgid "This sudoku can be solved."
msgstr "Este sudoku dase resolto."

#: pythonsudoku/gui.py:650 pythonsudoku/gui.py:653
msgid "This sudoku can't be solved."
msgstr "Este sudoku non se pode resolver."

#: pythonsudoku/info.py:18
msgid ""
"Sudoku, sometimes spelled Su Doku, is a placement puzzle, also known as Number Place in the United States. The aim of the puzzle is to enter a numeral from 1 through 9 in each cell of a grid, most frequently a 9x9 grid made up of 3x3 subgrids (called \"regions\"), starting with various numerals given in some cells (the \"givens\"). Each row, column and region must contain only one instance of each numeral. Completing the puzzle requires patience and logical ability. Its grid layout is reminiscent of other newspaper puzzles like crosswords and chess problems. Sudoku initially became popular in Japan in 1986 and attained international popularity in 2005.\n"
"\n"
"More information in http://en.wikipedia.org/wiki/Sudoku"
msgstr ""
"Sudoku é un quebracabezas matemático que se inventou nos Estados Unidos, fíxose popular en Xapón en 1986 e que se deu a coñecer internacionalmente en 2005. O obxectivo é encher, unha grella de 9×9 celas dividida en subgrellas de 3×3, das cifras do 1 ao 9 partindo dalgúns números xa colocados nalgunhas das celas. Non se pode repetir ningunha cifra nunha mesma ringleira, columna ou rexión. Un sudoku está ben deseñado se a solución é única. Resolver o problema require tempo e certas dotes lóxicas.\n"
"Máis información en http://gl.wikipedia.org/wiki/Sudoku"

#: pythonsudoku/printer.py:33
msgid "Print command not set"
msgstr "Comando de impresión non establecido"

#: pythonsudoku/text.py:87
msgid "sudoku with wrong difficulty!"
msgstr "sudoku coa dificultade equivocada!"

#: pythonsudoku/text.py:90 pythonsudoku/text.py:119
msgid "success!"
msgstr "¡conseguido!"

#: pythonsudoku/text.py:112
msgid "Solving sudoku..."
msgstr "Resolvendo sudoku..."

#: pythonsudoku/text.py:122
msgid "can't be solved!"
msgstr "¡non se pode resolver!"

#: pythonsudoku/text.py:138
msgid "The difficulty of the sudoku is..."
msgstr "A dificultade do sudoku é..."
